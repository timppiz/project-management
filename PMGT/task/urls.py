
from django.urls import path

from task.views import projectListView, projectDetailView, projectCreateView, projectUpdateView, projectDeleteView, \
    taskCreateView, taskUpdateView, taskDeleteView

urlpatterns = [
    path('',projectListView.as_view(), name = 'project-home' ),
    path('project/<int:pk>/',projectDetailView.as_view(), name = 'project-detail' ),
    path('project/new/',projectCreateView.as_view(), name = 'project-create' ),
    path('project/<int:pk>/update',projectUpdateView.as_view(), name = 'project-update' ),
    path('project/<int:pk>/delete',projectDeleteView.as_view(), name = 'project-delete' ),

    path('task/new/',taskCreateView.as_view(), name = 'task-create' ),
    path('task/<int:pk>/update',taskUpdateView.as_view(), name = 'task-update' ),
    # path('task/<int:pk>/prog',taskProgView.as_view(), name = 'task-prog' ),
    path('task/<int:pk>/delete',taskDeleteView.as_view(), name = 'task-delete' ),



]
