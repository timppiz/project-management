from _ast import Add
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models import Count
from django.db.models import Sum
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from task.models import Project, task



class projectListView(ListView):
    model = Project
    template_name = 'task/home.html'
    base_template_name = 'task/base.html'
    context_object_name = 'projects'


class projectDetailView(DetailView):
    model = Project
    template_name = 'task/project_detail.html'
    other_template_name = 'task/base.html'
    home_template_name = 'task/home.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tasks'] = task.objects.filter(project=self.kwargs['pk'])
        context['count'] = task.objects.filter(project=self.kwargs['pk']).annotate(
            num_comments=Count('task_name')
        )
        context['under_process'] = context['count'].filter(task_prog__lt = 100)
        context['complete'] = context['count'].filter(task_prog = 100)
        context['total'] = task.objects.filter(project=self.kwargs['pk']).aggregate(Sum('task_price')).get('task_price__sum')
        return context





class projectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    fields = ['title','date_posted', 'description']
    template_name = 'task/project_creat.html'
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class projectUpdateView(LoginRequiredMixin, UpdateView):
    model = Project
    fields = ['title','date_posted', 'description']
    template_name = 'task/project_creat.html'



class projectDeleteView(LoginRequiredMixin,  DeleteView):
    model = Project
    template_name = 'task/project_delete.html'
    success_url = '/'



# Create your Task views here.


class taskCreateView(LoginRequiredMixin,CreateView):
    model = task
    fields = ['project',  'task_name', 'plan_start_date','plan_end_date', 'task_price']
    template_name = 'task/task_create.html'


class taskUpdateView(LoginRequiredMixin,UpdateView):
    model = task
    fields = ['task_name', 'plan_start_date','plan_end_date', 'task_price', 'task_prog', 'actual_start_date','actual_end_date']
    template_name = 'task/task_create.html'



class taskDeleteView(LoginRequiredMixin,DeleteView):
    model = task
    template_name = 'task/task_delete.html'
    success_url = '/'